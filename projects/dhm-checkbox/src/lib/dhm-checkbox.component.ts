import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component( {
  template: `
    <span>
			<span *ngIf="isChecked" (click)="doToggle()">
				<i class="fa-check-square {{iconSize}}" [ngClass]="solid ? 'fas' : 'far'"></i>
			</span>
      <span *ngIf="!isChecked" (click)="doToggle()">
				<i class="fa-square {{iconSize}}" [ngClass]="solid ? 'fas' : 'far'"></i>
			</span>
    </span>
  `,
  selector: 'dhm-checkbox',
  styles: [
    `span {
      cursor: pointer;
    }`,
    `span:hover {
      color: darkgray;
    }`
  ]
} )
export class DhmCheckboxComponent {
  @Input() enableToggle = true;
  @Input() solid = true;
  @Input() iconSize = '';
  @Output() isCheckedChange = new EventEmitter<boolean>();

  constructor() {
  }

  // tslint:disable-next-line:variable-name
  protected _isChecked = false;

  get isChecked(): boolean {
    return this._isChecked;
  }

  @Input()
  set isChecked( theisChecked: boolean ) {
    this._isChecked = theisChecked;
    this.isCheckedChange.emit( theisChecked );
  }

  doToggle(): void {
    if ( this.enableToggle ) {
      this.isChecked = !this.isChecked;
    }
  }
}



