import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DhmCheckboxComponent } from './dhm-checkbox.component';

describe('DhmCheckboxComponent', () => {
  let component: DhmCheckboxComponent;
  let fixture: ComponentFixture<DhmCheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DhmCheckboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DhmCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
