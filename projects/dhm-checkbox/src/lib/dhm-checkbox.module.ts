import {NgModule} from '@angular/core';
import {DhmCheckboxComponent} from './dhm-checkbox.component';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';


@NgModule( {
  declarations: [
    DhmCheckboxComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    DhmCheckboxComponent
  ]
} )
export class DhmCheckboxModule {
}
