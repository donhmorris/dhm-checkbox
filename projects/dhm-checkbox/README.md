# dhm-checkbox

A better looking checkbox for Angular apps.

## Features

- Can use a filled or non-filled checkbox.
- Takes on the color of the parent element.
- Size can be controlled vis Font Awesome sizing classes (fa-2x)

[Documentation](https://bitbucket.org/donhmorris/dhm-checkbox/wiki/Home)
